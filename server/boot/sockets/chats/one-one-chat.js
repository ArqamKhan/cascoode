// var FCM = require('fcm-push');
let _ = require('lodash');
// This is one to one chat
module.exports = async function (io, app) {
  let chatHistory = app.models.ChatHistory;
  let customUser = app.models.CustomUser;

  io.on('connection', async function (socket) {
    console.log('connection called...!');
    // socket.on('isActiveConnection', async function (customUserId) {
    //   console.log('isActiveConnection called...!');
    //   await customUser.updateAll({
    //     id: customUserId
    //   }, {
    //     isActive: true
    //   });
    // });
    socket.on('chatMessage', async function (userObj) {
      console.log('chatMessage called...!');
      console.log(userObj);
      await chatHistory.create(userObj); // fileUrl, text, receiverId, date, customUserId
      let isActiveInstance = await customUser.find({
        where: {
          id: userObj.receiverId
        }
      });

      let isActiveVar;
      isActiveVar = _.find(isActiveInstance).isActive;
      isActiveVar = true; // Here active is manually set to true. FCM is not implemented yet.
      if (isActiveInstance.length > 0 && isActiveVar === true) {
        let instance = await customUser.find({
          where: {
            id: userObj.customUserId
          }
        });
        if (instance.length > 0) {
          console.log('in ONE-ONE chat message');

          socket.join(userObj.customUserId);
          io.emit('chatMessageEmitter', userObj);

        } else {
          return false;
        }
      }
    });

    // socket.on('isActiveDisconnection', async function (customUserId) {

    //   let socketDisconnectInstance = await customUser.find({
    //     where: {
    //       id: customUserId
    //     }
    //   });
    //   if (socketDisconnectInstance.length > 0) {
    //     await customUser.updateAll({
    //       id: customUserId,
    //     }, {
    //       isActive: false
    //     });
    //   }
    socket.on('disconnection', async function () {
      socket.disconnect();
    });
    // });
  });
};
