import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddNewMemberComponent } from '../team/add-new-member/add-new-member.component'

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Add-New-Member"
    },
    children: [
      {
        path: "",
        component: AddNewMemberComponent,

      }
      // ,
      // {
      //   path: "all-users",
      //   component: AllUsersComponent,
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddNewMemberRoutingModule { }
