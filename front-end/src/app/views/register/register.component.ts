import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import {HttpClient,HttpErrorResponse} from '@angular/common/http';
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { LoopBackConfig } from '../service/lb.config'

@Component({
  selector: "app-dashboard",
  templateUrl: "register.component.html"
})
export class RegisterComponent implements OnInit {
  registrationForm: FormGroup;
  showRoles;
  url: string = LoopBackConfig.getPath() + "/";
  _url: string = this.url +`api/custom-roles/show-all-roles`;
  _registrationURL: string = this.url +`api/custom-users`;
  submitted = false;
  userData;

  constructor(private _formBuilder: FormBuilder,
    private http: HttpClient) {}
  
    onClickSubmit(data)
  {
    this.submitted = true;
    console.log("enter in submit");
    console.log(data);
   
    if(this.registrationForm.invalid)
    {
      return;
    }
    // if(data.userName === '' || data.email === '' || data.password === '' || 
    // data.confirmPassword === '' || data.phoneNo === '' || data.roles === '')
    // {
    //   console.log("form data is required");
    // }
    else if(data.password !== data.confirmPassword)
    {
      console.log("password are not matched");
    }
    else {
      this.userData = {
        username: data.userName,
        email: data.email,
        phoneNo: data.phoneNo,
        password: data.password,
        customRoleId: data.roleId
      }
      console.log("form submited");
      console.log(this.userData);
      this.http.post(this._registrationURL,this.userData)
      .toPromise()
      .then(response =>{
        console.log("data submitted");
      })
      .catch((err: HttpErrorResponse) =>{
        console.log("error occuer");
        console.log(err.status);
      });
      
    }
  }

  get f(){
    return this.registrationForm.controls;
  }

  ngOnInit() {
    this.http.get(this._url)
    .toPromise()
    .then(response => {
      this.showRoles = response;
    })
    .catch((err: HttpErrorResponse) => {
      console.log("error occour");
      console.log(err.status);
    });
    this.registrationForm = this._formBuilder.group({
      userName: ['', Validators.required],
      email: ['', [ Validators.required, Validators.email]],
      phoneNo: ['', Validators.required],
      roleId: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    });
  }
}
