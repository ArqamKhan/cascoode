import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators, NgForm } from "@angular/forms";
import { Socket } from 'ngx-socket-io';
import {
  HttpClient,
  HttpErrorResponse,
  HttpResponse
} from "@angular/common/http";
import { AuthService } from '../service/Auth/login.service';
import { Injectable } from '@angular/core';
import { LoopBackConfig } from '../service/lb.config';

@Injectable({
  providedIn: 'root'
})
@Component({
  selector: "app-dashboard",
  templateUrl: "login.component.html"
})
export class LoginComponent implements OnInit {
  loginError=''
  userObj;
  _url: string = LoopBackConfig.getPath() + "/" ;
  loginUrl= this._url + `api/custom-users/login`;

  constructor(private http: HttpClient,private router:Router ,private auth:AuthService,
    private socket: Socket
    ) {}
  ngOnInit() {}
  onSubmit(user:NgForm){
    if(this.validateEmail(user.value.username))
    {
      this.http.post(this.loginUrl,{email:user.value.username , password:user.value.password})
      .subscribe((res)=>{
        this.auth.loggIn();
        this.userObj = res;
        localStorage.setItem("userId",this.userObj.userId);
        this.socket.emit("connection");
        this.socket.emit("isActiveConnection",this.userObj.userId);

        this.router.navigate(['/dashboard'])}

        ,(err)=>{this.loginError=err.error.error.message}
      )}
    else
    {
      this.http.post(this.loginUrl,{username:user.value.username , password:user.value.password})
      .subscribe(res=>{
        this.auth.loggIn();
        this.userObj = res;
        localStorage.setItem("userId",this.userObj.userId);
        this.socket.emit("connection");
        this.socket.emit("isActiveConnection",this.userObj.userId);
        this.router.navigate(['/dashboard']);
      },
      (err)=>{
        this.loginError=err.error.error.message;
      })
    }

  }

 validateEmail(email) {
      var re = /\S+@\S+\.\S+/;
      return re.test(email);
  }
}
